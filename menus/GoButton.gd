extends Button

var player_name: String

func set_player_name(s: String):
	disabled = s.empty()
	player_name = s

func _pressed():
	yield(Net.put_user(player_name), "completed")
	get_tree().change_scene("res://scenes/base/Base.tscn")
