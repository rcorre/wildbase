extends Node

const SSL := true
const URL := "https://gwj-server.herokuapp.com/v1/"
const AUTH_PATH := "user://auth"

var auth: String

class Request:
	extends HTTPRequest

	var method: int
	var path: String
	var auth: String
	var body

	signal completed(result)
	signal failed

	func _init(p_method: int, p_path: String, p_auth: String = "", p_body = null):
		._init()
		method = p_method
		path = p_path
		auth = p_auth
		body = p_body

	func _ready():
		connect("request_completed", self, "_on_completed")

		var headers := PoolStringArray(["Authorization: %s" % auth])
		var err := request(URL + path, headers, SSL, method, to_json(body))
		if err != OK:
			printerr("Failed to make request: %d" % err)
			queue_free()

		print("Made request to ", URL + path)

	func _on_completed(result: int, code: int, _headers: PoolStringArray, resp: PoolByteArray):
		prints("Got response from", URL + path, resp)
		queue_free()  # no matter what, free when we're done here

		if result != HTTPRequest.RESULT_SUCCESS:
			printerr("HTTP result %d != %d" % [result, HTTPRequest.RESULT_SUCCESS])
			emit_signal("failed")
		elif code != 200:
			var utf8 := resp.get_string_from_utf8()
			printerr("HTTP response %d != %d: %s" % [code, 200, utf8])
			emit_signal("failed")
		else:
			var utf8 := resp.get_string_from_utf8()
			emit_signal("completed", parse_json(utf8))

func _ready():
	var f := File.new()
	if f.file_exists(AUTH_PATH):
		f.open(AUTH_PATH, File.READ)
		auth = f.get_as_text()
		f.close()
		print("read auth")

func player_name() -> String:
	if auth.empty():
		printerr("auth not set")
		return ""
	return auth.split(":")[0]

func get_users():
	var req := Request.new(HTTPClient.METHOD_GET, "users")
	add_child(req)
	return req

func put_user(name: String) -> Request:
	var req := Request.new(HTTPClient.METHOD_PUT, "users/" + name)
	add_child(req)
	req.connect("completed", self, "_on_user_created", [name])
	return req

func _on_user_created(result: String, name: String):
	auth = name + ":" + result
	var f := File.new()
	f.open(AUTH_PATH, File.WRITE)
	f.store_string(auth)
	f.close()
	print("saved auth")
