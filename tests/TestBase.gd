extends Node

var failed := false
var base := Spatial.new()

func test(idx: int, expected: Vector3):
	var actual = base.call("player_idx_to_coord", idx)
	if not actual.is_equal_approx(expected):
		failed = true
		printerr("coord(%d) = %s (expected %s)" % [idx, actual, expected])

func _ready():
	base.set_script(preload("res://scenes/base/Base.gd"))
	get_tree().current_scene.add_child(base)
	# 9  10  11  12  13
	# 19  1   2   3  20
	# 21  7   0   8  22
	# 23  4   5   6  24
	# 14 15  16  17  18
	test(1, Vector3(-50, 0, -50))
	test(2, Vector3(0, 0, -50))
	test(3, Vector3(50, 0, -50))
	test(4, Vector3(-50, 0, 50))
	test(5, Vector3(0, 0, 50))
	test(6, Vector3(50, 0, 50))
	test(7, Vector3(-50, 0, 0))
	test(8, Vector3(50, 0, 0))
	test(9, Vector3(-100, 0, -100))
	test(12, Vector3(50, 0, -100))
	test(13, Vector3(100, 0, -100))
	test(23, Vector3(-100, 0, 50))
	test(24, Vector3(100, 0, 50))
	test(25, Vector3(-150, 0, -150))
	assert(not failed)
	print("OK!")
