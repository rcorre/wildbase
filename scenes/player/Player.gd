extends KinematicBody

const MAX_SPEED := 8.0
const ACCEL := 10.0
const GRAVITY := 10.0
const JUMP_SPEED := 10.0
const MOUSE_SENSITIVITY := 0.002

onready var camera_pivot: Spatial = $CameraPivot

var velocity := Vector3.ZERO
var look_target := Vector2.ZERO
var look_dir := Vector2.ZERO

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _input(ev: InputEvent):
	var motion := ev as InputEventMouseMotion
	if motion:
		look_target.x = look_target.x - motion.relative.x * MOUSE_SENSITIVITY
		look_target.y = clamp(look_target.y - motion.relative.y * MOUSE_SENSITIVITY, -PI / 2, PI / 2)

func _physics_process(delta: float):
	var move_target := global_transform.basis.xform(Vector3(
		Input.get_action_strength("Move Right") - Input.get_action_strength("Move Left"),
		0,
		Input.get_action_strength("Move Back") - Input.get_action_strength("Move Forward")
	)) * MAX_SPEED
	velocity.x = move_toward(velocity.x, move_target.x, ACCEL * delta)
	velocity.z = move_toward(velocity.z, move_target.z, ACCEL * delta)
	velocity.y -= GRAVITY * delta
	if Input.is_action_just_pressed("Jump") and is_on_floor():
		velocity.y = JUMP_SPEED
	velocity = move_and_slide(velocity, Vector3.UP)
	look_dir = Vector2(
		lerp(look_dir.x, look_target.x, 0.2),
		lerp(look_dir.y, look_target.y, 0.2)
	)
	rotation.y = look_dir.x
	camera_pivot.rotation.x = look_dir.y
