extends Spatial

const SPACING := 50.0
const HOME_SCENE := preload("res://scenes/home/Home.tscn")
const PLAYER_SCENE := preload("res://scenes/player/Player.tscn")

func base_rotation(pos: Vector3) -> float:
	if pos.z < 0:
		return -PI / 2
	elif pos.z > 0:
		return PI / 2
	elif pos.x > 0:
		return PI
	return 0.0

# returns the position of the base for the given player index
# player indices are positive integers starting at 1
# bases are positioned in concentric rings around the hub
func player_idx_to_coord(idx: int) -> Vector3:
	var ring: int = (int(sqrt(idx)) + 1) / 2
	var side_len := 2 * ring + 1 # 3, 5, 7, 9, ... each ring grows by 2
	var top_left := int(pow(2 * ring - 1, 2))
	var offset := idx - top_left
	if offset < side_len:
		# top side of ring
		return Vector3(offset - side_len / 2, 0, -ring) * SPACING
	elif offset < side_len * 2:
		# bottom side of ring
		return Vector3(offset - side_len - 1, 0, ring) * SPACING
	else:
		# left/right side of ring
		var x := (offset % 2) * (side_len - 1) - ring
		var y := (offset / 2) - side_len - ring + 1
		return Vector3(x, 0, y) * SPACING

func _ready():
	Net.get_users().connect("completed", self, "populate_homes")

func populate_homes(player_names: Array):
	for i in range(len(player_names)):
		prints("Created home for user", i + 1, player_names[i])
		var pos := player_idx_to_coord(i + 1) # player indices start at 1
		var home: Spatial = HOME_SCENE.instance()
		var name: String = player_names[i]
		add_child(home)
		home.transform.origin = pos
		home.rotation.y = base_rotation(pos)
		if name == Net.player_name():
			var player: Spatial = PLAYER_SCENE.instance()
			add_child(player)
			player.transform.origin = pos
			player.rotation.y = base_rotation(pos)
